package ru.am;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String path = ""; // <---- Сюда путь до файла
        String str = "";

        try (FileInputStream input = new FileInputStream(path)){
            while (input.available() > 0) str = str.concat(String.valueOf((char) input.read()));
            if (str.length() > 10) {
                throw new LengthGreaterThan10Characters();
            }
            System.out.println(str);
        } catch (IOException e) {
            System.out.println("Файл не прочитан.");
        } catch (LengthGreaterThan10Characters e) {
            System.out.println(e.getMessage());
        }
    }
}