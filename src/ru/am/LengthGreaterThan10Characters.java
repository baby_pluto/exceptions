package ru.am;

class LengthGreaterThan10Characters extends Exception {

    public LengthGreaterThan10Characters() {

        super("Длина строки больше 10 символов.");
    }
}